import React from "react";
import "./pickone.css";

const PickOne = () => {
  return (
    <div className="bg__main">
      <div class="box4">
  Prizes will be processed within 15 mins after the game.
</div>
    <div class="box grid">
  <div class="event-title-card">
    <div class="frame-59">
      <div class="frame-28">
        <div class="day">DAY 1 | SPECTRAL FURY</div>
        <div class="event-date">DECEMBER 27, 2022 | 1PM EVENT</div>
       
      </div>
    </div>
    <div class="frame-56">
      <div class="open-for-betting">OPEN FOR BETTING</div>
    </div>
    <div class="frame-60">
      <div class="frame-58">
        <div class="match">SHANE VAN BOENING VS DENNIS ORCOLLO</div>
      </div>
    </div>
  </div>
</div>
<div class="box1">
  <div class="event-title-card">
    <div class="frame-59">
      <div class="frame-28">
        <div class="day">DAY 1 | BATTLE SIEGE</div>
        <div class="event-date">DECEMBER 27, 2022 | 1PM EVENT</div>
        <div class="arrow-icon">
     
        </div>
      </div>
    </div>
    <div class="frame-56">
      <div class="open-for-betting">OPEN FOR BETTING</div>
    </div>
    <div class="frame-60">
      <div class="frame-58">
        <div class="match">SHANE VAN BOENING VS DENNIS ORCOLLO</div>
      </div>
    </div>
  </div>
</div>
<div class="box2">
  <div class="event-title-card">
    <div class="frame-59">
      <div class="frame-28">
        <div class="day">DAY 2 | BATTLE SIEGE</div>
        <div class="event-date">DECEMBER 27, 2022 | 1PM EVENT</div>
        <div class="arrow-icon">
     
        </div>
      </div>
    </div>
    <div class="frame-56">
      <div class="open-for-betting">OPEN FOR BETTING</div>
    </div>
    <div class="frame-60">
      <div class="frame-58">
        <div class="match">SHANE VAN BOENING VS DENNIS ORCOLLO</div>
      </div>
    </div>
  </div>
</div>
<div class="box3">
  <div class="event-title-card">
    <div class="frame-59">
      <div class="frame-28">
        <div class="day">10 MAN RING</div>
        <div class="event-date">DECEMBER 27, 2022 | 1PM EVENT</div>
        <div class="arrow-icon">
     
        </div>
      </div>
    </div>  
    <div class="frame-56">
      <div class="open-for-betting">OPEN FOR BETTING</div>
    </div>
    <div class="frame-60">
      <div class="frame-58">
        <div class="match">SHANE VAN BOENING VS DENNIS ORCOLLO</div>
      </div>
    </div>
  </div>
</div>

</div>
  )
}

export default PickOne;
