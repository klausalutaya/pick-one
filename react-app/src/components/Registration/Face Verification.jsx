import React from 'react';
import { useHistory } from 'react-router-dom'; // Assuming you are using React Router for navigation
import babae from './Ellipse 43.png';

const FaceVerification = ({ handleNextPage, handlePrevPage }) => {
  const history = useHistory();

  const goBackToCase3 = () => {
    handlePrevPage(3);
  };

  return (
    <>
      <form>
        <h1 className="register__title2">FACE VERIFICATION</h1>
        <img className="babae" src={babae} alt="" />
        <h3 className="selfie">Take a selfie to verify your account</h3>
        <p className="facial">
          Your facial Information will be collected through the verification process.
        </p>
      </form>
      <div className="previous-button" onClick={goBackToCase3}>
        <img src={arrowLeftIcon} alt="Previous" className="arrow-icon" style={{ fill: 'white' }} />
      </div>
    </>
  );
};

export default FaceVerification;
